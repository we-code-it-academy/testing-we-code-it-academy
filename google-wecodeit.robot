*** Settings ***
Documentation     Simple example using SeleniumLibrary.
Library           SeleniumLibrary
Suite Setup      Go to Google
Suite Teardown  Close All Browsers 

*** Variables ***
${HOMEPAGE}      https://www.google.com
${BROWSER}        Chrome

***Test Case***
Google wecodeit and find WeCodeIt
    Google and Check Results    wecodeit.academy    wecodeit.academy

***Keywords***
Go to Google
    Open Browser    ${HOMEPAGE}    ${BROWSER}
Google and Check Results
    [Arguments]     ${searchkey}     ${result}
    Input Text      class=gLFyf.gsfi    ${searchkey}
    Click Button   css=center:nth-child(1) > .gNO89b
    Wait Until Page Contains    ${result}
    
Go to homepage
    Open Browser    ${HOMEPAGE}     ${BROWSER}