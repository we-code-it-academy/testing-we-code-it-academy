# Testing WeCodeIt.academy

testing http://wecodeit.academy using robotframework and seleniumlibrary.

Robotframework: http://robotframework.org

Robotframework-github: https://github.com/robotframework

SelenniumLibrary-github: https://github.com/robotframework/SeleniumLibrary

## Required installations (Windows)

1.  [Download Python](https://www.python.org/downloads/)

2.  [Install python virtualenvironment](https://docs.python.org/3/tutorial/venv.html)

3.  Execute in Powershell/Bash/CMD

```
python -m venv virtualenv
virtualenv\Scripts\activate.bat
pip install -r requirements.txt
webdrivermanager firefox chrome --linkpath /usr/local/bin
```

## Running Tests

```
robot google-wecodeit.robot
robot wecodeit-test.robot
``` 