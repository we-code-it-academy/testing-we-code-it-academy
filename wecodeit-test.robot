*** Settings ***
Documentation   Robotframework Project to Test wecodeit.academy
Library         SeleniumLibrary
Test Setup      Go TO Wecodeit.academy
Test Teardown   Close Browser

*** Variables ***
${BROWSER}              Chrome
${wecoeit url}          http://wecodeit.academy
${wecodeit title}       We code IT Academy - Die IT Akademie für Flüchtlinge & MigrantInnen
${wecodeit blog title}  Blog
${we-integrate url}     https://www.we-integrate.de/
${we-integrate title}   Home | we integrate e.V.

*** Test Cases ***

Wecoeit Testing
    First H2 Should Be              We code IT Academy
    Wecodeit Should Contin Link     https://bit.ly/3qyuSbo
    Go To We-Integrate  
    We-Integrate Should Be Open 
    Return To Wecodeit.Academy
    Check Wecodeit Logo Reference
    Click Wecodeit Home
    Wecodeit Home Page Should Open
    Go To Wecodeit Blog
    Wecodeit Blog Should Open



*** Keywords ***

Go TO Wecodeit.academy
    open browser    ${wecoeit url}     ${BROWSER}

First H2 Should Be
    [Arguments]     ${searhcwords}
    @{element} =    Get WebElements     css=h2 
    Element Should Contain      @{element}      ${searhcwords}

Wecodeit Should Contin Link
    [Arguments]     ${link}    
    Page Should Contain Link    ${link}  

Go To We-Integrate
    Go To                ${we-integrate url}

We-Integrate Should Be Open
    Title Should Be      ${we-integrate title}

Return To Wecodeit.Academy
    Go Back

Check Wecodeit Logo Reference
    Element Attribute Value Should Be   css:a.navbar-brand      href    https://wecodeit.academy/

Click Wecodeit Home
    Click Link          css=.nav-item:nth-child(1) > a

Wecodeit Home Page Should Open
    Title Should Be     ${wecodeit title}

Go To Wecodeit Blog
    Click LInk          css=.nav-item:nth-child(2) > a

Wecodeit Blog Should Open
    Title Should Be     ${wecodeit blog title}



